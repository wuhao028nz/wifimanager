import React from 'react';
import Login from "./src/component/Login";
import ScanView from "./src/component/ScanView";
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import BLEConnectView from "./src/component/BLEConnectView";
import IoTmsgView from "./src/component/IoTmsgView";
import SelectCompanyView from "./src/component/SelectCompanyView";
import SelectVehicleView from "./src/component/SelectVehicleView";

import * as Sentry from '@sentry/react-native';

Sentry.init({
    dsn: 'https://1ca249162f524a698ad8e36972399e65@o334944.ingest.sentry.io/5370316',
});

const AppNavigator = createStackNavigator({
    Login :{
        screen:Login,
        navigationOptions: {
            header: null,
        },
    },
    SelectCompanyView :{
        screen:SelectCompanyView,
        navigationOptions: {
            header: null,
        },
    },
    SelectVehicleView :{
        screen:SelectVehicleView,
        navigationOptions: {
            header: null,
        },
    },
    IoTmsgView :{
        screen:IoTmsgView,
        navigationOptions: {
            header: null,
        },
    },
    BLEConnectView :{
        screen:BLEConnectView,
        navigationOptions: {
            header: null,
        },
    },
},{
    initialRouteName:"Login"
});

const App = createAppContainer(AppNavigator);

export default App;
