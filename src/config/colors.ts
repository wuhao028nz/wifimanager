const colors = {
    BLACK: "#000",
    WHITE: "#FFF",
    DODGER_BLUE: "#428AF8",
    SILVER: "#BEBEBE",
    TORCH_RED: "#F8262F",
    MISCHKA: "#E5E4E6",
    CORETEX: "#f26522",
    DARKGREY: "#A9A9A9",
    LIGHT_GREY: '#f6f6f6',
};

export default colors;
