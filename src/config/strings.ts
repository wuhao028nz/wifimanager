const strings = {
    LOGIN: "Login",
    WELCOME_TO_LOGIN: "Welcome to the login screen!",
    EMAIL_PLACEHOLDER: "User name",
    PASSWORD_PLACEHOLDER: "Password",
    MAC_ADDRESS: "macAddress",
    REMEMBER_ME: "Remember me",
    REMEMBER_KEY: "Remember",
    WiFiMacaddress: "macAddress",
    SESSION_ID: "SessionID",
    COMPANY_ID: "CompanyID",
    TRACK_ID: "TrackID",
    INVALID_ACCESS: "Invalid username or password",
};

export default strings;
