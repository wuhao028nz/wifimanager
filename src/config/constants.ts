import {Platform} from "react-native";

const constants = {
    IS_ANDROID: Platform.OS === "android",
    IS_IOS: Platform.OS === "ios",
    API_360: "https://coretexnarestapi.azurewebsites.net",
    // API_360: "https://coretexwebapisfqa.azurewebsites.net",   //qa
    REACT_APP_COREHUB_SUPPORT_API: "https://corehub-support-tool-api.azurewebsites.net/api/",
    // REACT_APP_COREHUB_SUPPORT_API: "https://CorehubRemoteSupportQA.azurewebsites.net",   //qa
    IS_DEBUG_MODE_ENABLED: Boolean(window.navigator.userAgent),
    SERVICE_BLE_UUID: '6e400001-b5a3-f393-e0a9-e50e24dcca9e',
    CHARACTERISTIC_READ_UUID: '6e400003-b5a3-f393-e0a9-e50e24dcca9e',
    CHARACTERISTIC_WRITE_UUID: '6e400002-b5a3-f393-e0a9-e50e24dcca9e',
    PRIMARY_FLEET_ID: '22222222-2222-2222-2222-222222222222',
};

export default constants;
