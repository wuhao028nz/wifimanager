import React from 'react';
import {
    View,
    StyleSheet,
    Animated,
    Image,
    Dimensions,
} from 'react-native';
import colors from "../config/colors";
import logoImg from '../images/logo_icon.png';

export default class AnimatedSpread extends React.Component {
    static defaultProps = {
        ripple: 3,
        initialDiameter: 120,
        endDiameter: 250,
        initialPosition: {top: 250, left: 0},
        rippleColor: colors.DODGER_BLUE,
        intervals: 500,
        spreadSpeed: 2000
    };

    constructor(props) {
        super(props);
        const isPortrait = () => {
            const dim = Dimensions.get('screen');
            return dim.height >= dim.width;
        };

        this.state = {
            orientation: isPortrait() ? 'portrait' : 'landscape',
        };

        // Event Listener for orientation changes
        Dimensions.addEventListener('change', () => {
            this.setState({
                orientation: isPortrait() ? 'portrait' : 'landscape'
            });
        });

        let rippleArr = [];
        for (let i = 0; i < props.ripple; i++) rippleArr.push(0);
        this.state = {
            anim: rippleArr.map(() => new Animated.Value(0))
        };
        this.cancelAnimated = false;
        this.animatedFun = null;
        this.startAnimation()
    }

    startAnimation() {
        this.state.anim.map((val, index) => val.setValue(0));
        this.animatedFun = Animated.stagger(this.props.intervals, this.state.anim.map((val) => {
            return Animated.timing(val,
                {
                    toValue: 1,
                    useNativeDriver: false,
                    duration: this.props.spreadSpeed,
                }
            )
        }));
        this.cancelAnimated = false;
        this.animatedFun.start(() => {
            if (!this.cancelAnimated) {
                this.startAnimation()
            }
        })
    }

    stopAnimation() {
        this.cancelAnimated = true;
        this.animatedFun.stop();
        this.state.anim.map((val, index) => val.setValue(0));
    }

    render() {
        const {initialPosition, initialDiameter, endDiameter, rippleColor} = this.props;
        let r = endDiameter - initialDiameter;
        let rippleComponent = this.state.anim.map((val, index) => {
            if (this.state.orientation === 'portrait') {
                return (
                    <Animated.View key={"animatedView_" + index}
                                   style={[styles.spreadCircle, {backgroundColor: rippleColor}, {
                                       opacity: val.interpolate({
                                           inputRange: [0, 1],
                                           outputRange: [1, 0]
                                       }),
                                       height: val.interpolate({
                                           inputRange: [0, 1],
                                           outputRange: [initialDiameter, endDiameter]
                                       }),
                                       width: val.interpolate({
                                           inputRange: [0, 1],
                                           outputRange: [initialDiameter, endDiameter]
                                       }),
                                       top: val.interpolate({
                                           inputRange: [0, 1],
                                           outputRange: [initialPosition.top - initialDiameter / 2, initialPosition.top - initialDiameter / 2 - r / 2]
                                       }),
                                       left: val.interpolate({
                                           inputRange: [0, 1],
                                           outputRange: [initialPosition.left - initialDiameter / 2, initialPosition.left - initialDiameter / 2 - r / 2]
                                       }),
                                   }]}></Animated.View>
                )
            } else {
                return (
                    <Animated.View key={"animatedView_" + index}
                                   style={[styles.spreadCircle, {backgroundColor: rippleColor}, {
                                       opacity: val.interpolate({
                                           inputRange: [0, 1],
                                           outputRange: [1, 0]
                                       }),
                                       height: val.interpolate({
                                           inputRange: [0, 1],
                                           outputRange: [initialDiameter, endDiameter]
                                       }),
                                       width: val.interpolate({
                                           inputRange: [0, 1],
                                           outputRange: [initialDiameter, endDiameter]
                                       }),
                                       top: val.interpolate({
                                           inputRange: [0, 1],
                                           outputRange: [150 - initialDiameter / 2, 150 - initialDiameter / 2 - r / 2]
                                       }),
                                       left: val.interpolate({
                                           inputRange: [0, 1],
                                           outputRange: [initialPosition.left - initialDiameter / 2, initialPosition.left - initialDiameter / 2 - r / 2]
                                       }),
                                   }]}></Animated.View>
                )
            }

        });
        if (this.state.orientation === 'portrait') {
            return (
                <View>
                    {rippleComponent}
                    <Image source={logoImg} style={styles.image}/>
                </View>
            )
        } else {
            return (
                <View>
                    {rippleComponent}
                    <Image source={logoImg} style={styles.imageLandscape}/>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    spreadCircle: {
        borderRadius: 999,
        position: 'absolute',
    },
    image: {
        width: "12%",
        position: 'absolute',
        alignItems: "center",
        resizeMode: "contain",
        alignSelf: "center",
        justifyContent: "space-between"
    },
    imageLandscape: {
        width: "6%",
        position: 'absolute',
        alignItems: "center",
        resizeMode: "contain",
        alignSelf: "center",
        top: -100,
        justifyContent: "space-between"
    },
});
