import React, {Component} from 'react';
import {
    FlatList,
    StyleSheet,
    View,
    Text,
    KeyboardAvoidingView,
    TouchableOpacity, RefreshControl, Image, Alert, Modal, TouchableHighlight,
} from 'react-native';
import colors from "../config/colors";
import strings from "../config/strings";
import constants from "../config/constants";
import {storage} from './storage/Storage'
import ApiService from "./util/ApiService";
import SearchBar from 'react-native-search-bar';
import returnIcon from "../images/return.png";
import {Header, Icon, Input, Item} from "native-base";
import {moderateScale} from "react-native-size-matters";
import {SkypeIndicator} from "react-native-indicators";
import px from "./util/PX";
import {SmallText, MediumText} from "./util/PX";

export default class SelectVehicleView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            vehicleList: [],
            vehicleToShow: [],
            modalVisible: false,
        };
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        this.setState({isLoading: true});
        storage.load(strings.COMPANY_ID, (companyID) => {
            storage.load(strings.SESSION_ID, (sessionID) => {
                this.fetchVehicle(companyID, sessionID)
            });
        });
    }

    async fetchVehicle(companyID, sessionID) {
        const vehicleList = await ApiService.getCorehubVehicleList(sessionID, companyID);
        if (typeof vehicleList === 'string') {
            await this.setState({isLoading: false});
            alert(vehicleList);
        } else if (vehicleList.Value) {
            await this.setState({vehicleList: vehicleList.Value});
            await this.setState({vehicleToShow: vehicleList.Value});
            await this.setState({isLoading: false});
        } else if (!vehicleList.Value) {
            await this.setState({isLoading: false});
            if (vehicleList.Code === 1) {
                this.sessionExpire()
            } else {
                alert(JSON.stringify(vehicleList));
            }
        }
    }

    sessionExpire() {
        Alert.alert(
            'Session Expired',
            "Login is required",
            [
                {
                    text: 'OK',
                    onPress: () => {
                        storage.save(strings.REMEMBER_KEY, false);
                        this.props.navigation.replace('Login');
                    },
                },
                {
                    text: 'Cancel',
                    style: 'cancel',
                    onPress: () => {

                    },
                },
            ],
            {cancelable: false},
        );
    }

    separator() {
        return <View style={{height: 1, backgroundColor: '#999999', marginLeft: 20}}/>;
    }

    createListHeader() {
        return (
            <View style={styles.headView}>
                <Text style={{color: 'white'}}>
                    Select your Vehicle
                </Text>
            </View>
        )
    }

    onItemClick(item) {
        storage.save(strings.TRACK_ID, item.TrackID);
        this.setState({modalVisible: true})
    }

    createListItem(item) {
        return (
            <TouchableOpacity activeOpacity={0.5} onPress={() => this.onItemClick(item)}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'flex-start',
                    justifyContent: 'flex-start',
                    marginTop: 5,
                    backgroundColor: '#fff',
                    padding: 10
                }}>
                    <Text style={{marginLeft: 6, padding: 2, fontSize: SmallText,}}>
                        {item.Name} , {item.TrackID}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    onChangeText(text) {
        if (typeof text === 'string' && text.length > 0) {
            const newArr = this.state.vehicleList.filter(item =>
                item.Name.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
                item.TrackID.toLowerCase().indexOf(text.toLowerCase()) > -1);
            this.setState({vehicleToShow: newArr})
        } else {
            this.setState({vehicleToShow: this.state.vehicleList})
        }
    }

    navReturn() {
        this.props.navigation.replace('SelectCompanyView');
    }

    render() {
        return (
            <KeyboardAvoidingView
                style={styles.container}
                behavior={constants.IS_IOS ? "padding" : "height"}
            >
                <View style={styles.container}>

                    <Header searchBar rounded style={styles.headView}>
                        <TouchableOpacity
                            style={styles.centerAligned}
                            transparent
                            onPress={() => {
                                this.navReturn()
                            }}>
                            <Icon
                                type="MaterialIcons"
                                name='arrow-back'
                                style={{fontSize: moderateScale(26)}}
                            />
                        </TouchableOpacity>
                        <Item
                            style={{
                                backgroundColor: colors.LIGHT_GREY,
                                borderRadius: moderateScale(25),
                                padding: moderateScale(2),
                            }}>
                            <Icon
                                type="MaterialIcons"
                                name="search"/>
                            <Input style={{fontSize: MediumText}}
                                   onChangeText={text => this.onChangeText(text)}
                                   placeholder="Search for your vehicle"
                                   value={this.state.value}
                            />
                        </Item>
                    </Header>

                    <FlatList style={styles.flatList}
                              data={this.state.vehicleToShow}
                              renderItem={({item}) => this.createListItem(item)}
                              ItemSeparatorComponent={this.separator}
                              refreshControl={
                                  <RefreshControl
                                      title={"Loading"}
                                      colors={[colors.CORETEX]}
                                      tintColor={colors.CORETEX}
                                      titleColor={colors.CORETEX}
                                      refreshing={false}
                                      onRefresh={() => {
                                          this.getData();
                                      }}
                                  />
                              }
                    />
                </View>
                {this.state.isLoading &&
                <View style={styles.loginIndicator}>
                    <SkypeIndicator size={moderateScale(80)} color={colors.CORETEX}/>
                </View>
                }
                {<Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setState({modalVisible: false})
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Text style={styles.modalText}>Please choose one way to connect to this Corehub
                                device</Text>
                            <TouchableHighlight
                                style={{...styles.openButton, backgroundColor: colors.CORETEX}}
                                onPress={() => {
                                    this.setState({
                                        modalVisible: !this.state.modalVisible
                                    });
                                    this.props.navigation.replace('BLEConnectView');
                                }}
                            >
                                <Text style={styles.textStyle}>BLE</Text>
                            </TouchableHighlight>

                            <TouchableHighlight
                                style={{...styles.openButton, backgroundColor: colors.CORETEX}}
                                onPress={() => {
                                    this.setState({
                                        modalVisible: !this.state.modalVisible
                                    });
                                    this.props.navigation.replace('IoTmsgView');
                                }}
                            >
                                <Text style={styles.textStyle}>Cloud</Text>
                            </TouchableHighlight>

                        </View>
                    </View>
                </Modal>
                }
            </KeyboardAvoidingView>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        backgroundColor: colors.WHITE,
        paddingTop: 0,
    },
    flatList: {
        width: '100%',
        backgroundColor: colors.WHITE,
    },
    headView: {
        width: '100%',
        height: px(50),
        backgroundColor: 'transparent',
    },
    searchBox: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
        fontSize: SmallText
    },
    openButton: {
        width: '100%',
        marginTop: 10,
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        marginLeft: '10%',
        marginRight: '10%'
    },
    textStyle: {
        width: 300,
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    centerAligned: {alignItems: 'center', justifyContent: 'center'},
    loginIndicator: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF88',
    },
});
