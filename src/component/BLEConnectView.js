import React, {Component} from 'react';
import {
    PermissionsAndroid, StyleSheet, Text, TouchableOpacity
    , View, Alert, Linking, Animated, TextInput, Image, Modal, TouchableHighlight
} from 'react-native';
import colors from "../config/colors";
import {storage} from "./storage/Storage";
import strings from "../config/strings";
import {BleManager} from "react-native-ble-plx";
import AnimatedSpread from "./AnimatedSpread";
import constants from "../config/constants";
import {Base64} from 'js-base64';
import wifi from 'react-native-android-wifi';
import Clipboard from "@react-native-community/clipboard";
import BackgroundTimer from 'react-native-background-timer';
import Slider from '@react-native-community/slider';
import * as DeviceInfo from "react-native-device-info";
import {Body, Header, Icon, Input, Item, Title} from "native-base";
import {moderateScale} from "react-native-size-matters";
import {ProgressStep, ProgressSteps} from "react-native-progress-steps";
import {SkypeIndicator} from "react-native-indicators";
import px, {MediumText, SmallText, LabelText} from "./util/PX";
import RadioGroup from "react-native-radio-buttons-group";
import NetInfo from "@react-native-community/netinfo";
import Info from "../images/info.png";

export default class BLEConnectView extends Component {
    constructor(props) {
        super(props);
        this.shakeAnimation = new Animated.Value(0);
        this.manager = new BleManager();
        this.state = {
            showModal: false,
            macAddress: " ",
            currDevice: null,
            qrCodeContent: "",
            deviceId: " ",
            isConnected: false,
            connectionStatus: "Searching for devices...",
            time: 0,
            maxDataUsage: 0,
            ssid: "",
            pass: "",
            isLoading: false,
            ProgressErrors: false,
            configHint: "",
            connectedToWifi: false,
            sessionSelected: 0,
            sessionData: [
                {
                    label: 'Temporary',
                    value: 'Temporary',
                    color: '#f26522'
                },
                {
                    label: 'Permanent',
                    value: 'Permanent',
                    color: '#f26522'
                }
            ],
        }
    }

    componentDidMount() {
        BackgroundTimer.stopBackgroundTimer();
        this.setState({
            connectedToWifi: false,
        });
        NetInfo.addEventListener(state => {
            console.log('Connection type', state.type);
            console.log('Is connected?', state.isConnected);
            console.log('local state connectedToWifi', this.state.connectedToWifi);
            if (state.type === 'wifi' && state.details.ssid === this.state.ssid) {
                console.log("Wifi ssid: " + state.details.ssid);
                BackgroundTimer.stopBackgroundTimer();
                this.setState({
                    connectionStatus: "Connected to hotspot: " + this.state.ssid,
                    isLoading: false,
                    connectedToWifi: true,
                });
                if (this.state.currDevice.id != null) {
                    this.manager?.cancelDeviceConnection(this.state.currDevice.id);
                }
            } else if (!state.isConnected && this.state.connectedToWifi) {
                this.setState({
                    connectedToWifi: false,
                }, this.alreadyDisconnect());
            } else {
                this.setState({connectedToWifi: false,});
            }
        });
        this.getBlePermission();
        try {
            storage.load(strings.WiFiMacaddress, (data) => {
                this.setState({macAddress: data})
            });
            storage.load(strings.TRACK_ID, (data) => {
                this.setState({deviceId: data});
                this.setState(
                    {connectionStatus: "Searching for " + data})
            });
            const subscription = this.manager.onStateChange((state) => {
                if (state === 'PoweredOn') {
                    this.scanAndConnect();
                    subscription.remove();
                } else {
                    this.manager.enable();
                    this.scanAndConnect();
                }
            }, true);
        } catch (e) {
            alert(e)
        }
    }

    componentWillUnmount() {
        BackgroundTimer.stopBackgroundTimer();
        const unsubscribe = NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
        });
        unsubscribe();
        this.manager.stopDeviceScan();
    }

    async scanAndConnect() {
        this.manager.startDeviceScan(null, null, async (error, device) => {
            if (error) {
                alert(error);
                this.manager.stopDeviceScan()
            }
            console.log("current in range: " + JSON.stringify(device?.name));
            this.setState(
                {
                    connectionStatus: "Searching for " +
                        this.state.deviceId + "\n  current in range: \n " + JSON.stringify(device?.name)
                });
            if (device?.name != null && device?.name === this.state.deviceId) {
                this.setState({
                    connectionStatus: "Found device " + JSON.stringify(device?.name) + ", trying to connect"
                });
                await this.manager.stopDeviceScan();
                try {
                    if (device.id != null) {
                        const deviceConnected = await this.manager.connectToDevice(
                            device.id,
                            {requestMTU: 517},
                        );
                        await this.setState({currDevice: deviceConnected});
                        if (this.state.currDevice !== null) {
                            await this.state.currDevice?.discoverAllServicesAndCharacteristics();
                            await this.setState({
                                isConnected: true,
                                connectionStatus: "Connected to " + JSON.stringify(device?.name),
                                // once connected via BLE, reset the configs
                                time: 0,
                                maxDataUsage: 0,
                            });
                        }
                    }
                } catch (err) {
                    this.retryScanBLE();
                    this.connectionError(err);
                }
            }
        });
    }

    getBlePermission = async () => {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
            {
                title: 'Location Permission for BLE',
                message:
                    'In order to use Bluetooth Low Energy, CoreHub Installer needs to access the device location. Do you wish to allow?',
                buttonNeutral: 'Ask Me Later',
                buttonNegative: 'Cancel',
                buttonPositive: 'OK',
            },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            // Check bluetooth state of device
            const state = await this.manager.state();
            if (state === 'PoweredOn') {
                console.log('BLE is PoweredOn');
            } else {
                await this.manager.enable();
            }
        } else {
            console.log('BLE permission denied');
        }
    };

    async connectWifi() {
        this.setState({isLoading: true});
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'Wifi networks',
                    'message': 'We need your permission in order to find wifi networks'
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("Thank you for your permission! :)");
            } else {
                console.log("You will not able to retrieve wifi available networks list");
            }
        } catch (err) {
            alert("connectWifi: " + err)
        }
        wifi.setEnabled(true);
        wifi.getSSID((ssid) => {
            if (ssid !== this.state.ssid) {
                this.keepConnecting()
            } else {
                if (this.state.currDevice.id != null) {
                    this.manager?.cancelDeviceConnection(this.state.currDevice.id);
                }
                this.setState({
                    connectionStatus: "Connected to hotspot: " + this.state.ssid,
                    isLoading: false,
                    connectedToWifi: true,
                });
            }
        });
    }

    keepConnecting() {
        BackgroundTimer.stopBackgroundTimer();
        wifi.setEnabled(true);
        BackgroundTimer.runBackgroundTimer(() => {
                wifi.getSSID((ssid) => {
                    if (ssid === this.state.ssid) {
                        BackgroundTimer.stopBackgroundTimer();
                        this.setState({
                            connectionStatus: "Connected to hotspot: " + this.state.ssid,
                            isLoading: false,
                            connectedToWifi: true,
                        });
                        if (this.state.currDevice.id != null) {
                            this.manager?.cancelDeviceConnection(this.state.currDevice.id);
                        }
                    } else {
                        wifi.disconnect();
                        wifi.reScanAndLoadWifiList((wifiStringList) => {
                        }, (error) => {
                            console.log(error);
                        });
                        wifi.findAndConnect(this.state.ssid, this.state.pass, (found) => {
                            if (found) {
                                this.setState({connectionStatus: "Hotspot " + this.state.ssid + " is in range"});
                            } else {
                                this.setState({
                                    connectionStatus: "Hotspot " + this.state.ssid + " is not in range," +
                                        " try again in few seconds"
                                });
                            }
                        });
                    }
                });
            },
            15000);
    }

    async sendCommand(time, MaxData) {
        this.setState({isLoading: true});
        try {
            console.log("config: time 1: " + time + " data: " + MaxData);
            this.sendCMDFetchConfig(time, MaxData)
        } catch (e) {
            this.setState({isLoading: false});
            this.connectionError(e);
        }
    }

    async sendCMDFetchConfig(time, maxData) {
        try {
            let brand = DeviceInfo.getBrand();
            let model = DeviceInfo.getModel();
            var macAddress =  DeviceInfo.getMacAddressSync();
            if(constants.IS_IOS){
                macAddress = this.state.macAddress;
            }
            await this.manager.writeCharacteristicWithoutResponseForDevice(
                this.state.currDevice.id,
                constants.SERVICE_BLE_UUID,
                constants.CHARACTERISTIC_WRITE_UUID,
                Base64.encode("hotspot:" + time * 60 + "|data:" + maxData + "|" + macAddress + "@" + model + "@" + brand),
            );
            console.log("set hotSpot config " +
                "hotspot:" + time * 60 + "|data:" + maxData + "|" + macAddress + "@" + model + "@" + brand);
            const data = await this.manager.readCharacteristicForDevice(
                this.state.currDevice.id,
                constants.SERVICE_BLE_UUID,
                constants.CHARACTERISTIC_READ_UUID,
            );
            let hotSpotInfo = await Base64.decode(data.value);
            console.log("get hotSpotInfo " + hotSpotInfo);
            console.log("get hotSpotInfo " + hotSpotInfo);
            if (hotSpotInfo !== null) {
                if (hotSpotInfo === 'active') {
                    BackgroundTimer.stopBackgroundTimer();
                    await this.manager.writeCharacteristicWithoutResponseForDevice(
                        this.state.currDevice.id,
                        constants.SERVICE_BLE_UUID,
                        constants.CHARACTERISTIC_WRITE_UUID,
                        Base64.encode("wifi:config"),
                    );
                    const data = await this.manager.readCharacteristicForDevice(
                        this.state.currDevice?.id,
                        constants.SERVICE_BLE_UUID,
                        constants.CHARACTERISTIC_READ_UUID,
                    );
                    let info = await Base64.decode(data.value);
                    console.log("get info " + info);
                    info = info.replace("ssid:", "").replace("active:true", "");
                    this.setState({
                        ssid: info.split("pass:")[0],
                        pass: info.split("pass:")[1],
                        isLoading: false,
                        connectionStatus: "Hotspot info fetched, SSID:" + info.split("pass:")[0] + " ,Password: " + info.split("pass:")[1]
                    });
                    this.ssidFetched();
                } else {
                    BackgroundTimer.stopBackgroundTimer();
                    BackgroundTimer.runBackgroundTimer(() => {
                        this.sendCMDFetchConfig(this.state.time, this.state.maxDataUsage);
                    }, 2000)
                }
            } else {
                this.setState({isLoading: true});
                this.ssidFetchedError();
            }
        } catch (e) {
            this.connectionError(e);
        }
    }

    ssidFetchedError() {
        Alert.alert(
            'Error',
            "error happened while fetching the SSID and password",
            [
                {
                    text: 'Try again',
                    onPress: () => {
                        this.sendCMDFetchConfig(this.state.time, this.state.maxDataUsage)
                    },
                },
                {
                    text: 'Cancel',
                    style: 'cancel',
                    onPress: () => {

                    },
                },
            ],
            {cancelable: false},
        );
    }

    ssidFetched() {
        Alert.alert(
            'Got hotspot info',
            "SSID:" + this.state.ssid + "  Password: " + this.state.pass + "\nCopy password to Clipboard ?",
            [
                {
                    text: 'OK',
                    onPress: () => {
                        Clipboard.setString(this.state.pass);
                    },
                },
                {
                    text: 'Cancel',
                    style: 'cancel',
                    onPress: () => {

                    },
                },
            ],
            {cancelable: false},
        );
    }

    connectionError(error) {
        Alert.alert(
            'Connection Error',
            'There was some error connecting to the CoreHub. Please scan again. \n error message:' + error,
            [
                {
                    text: 'Scan',
                    onPress: () => {
                        this.retryScanBLE();
                    },
                }
            ],
            {cancelable: true},
        );
    }

    alreadyDisconnect() {
        Alert.alert(
            'Disconnected',
            "You've disconnected from " + this.state.ssid,
            [
                {
                    text: 'set again',
                    onPress: () => {
                        this.retryScanBLE();
                    },
                },
                {
                    text: 'Cancel',
                    style: 'cancel',
                    onPress: () => {

                    },
                },
            ],
            {cancelable: false},
        );
    }

    retryScanBLE() {
        this.manager.enable();
        if (this.state.currDevice?.deviceId) {
            this.manager.cancelDeviceConnection(this.state.currDevice.deviceId);
        }
        this.state.currDevice?.cancelConnection();
        this.setState({currDevice: null});
        this.setState({isConnected: false});
        this.setState({connectionStatus: "Searching for " + this.state.deviceId});
        this.scanAndConnect();
    }

    navReturn() {
        this.props.navigation.replace('SelectVehicleView');
    }

    startShake = () => {
        Animated.sequence([
            Animated.timing(this.shakeAnimation, {toValue: 10, duration: 100, useNativeDriver: true}),
            Animated.timing(this.shakeAnimation, {toValue: -10, duration: 100, useNativeDriver: true}),
            Animated.timing(this.shakeAnimation, {toValue: 10, duration: 100, useNativeDriver: true}),
            Animated.timing(this.shakeAnimation, {toValue: 0, duration: 100, useNativeDriver: true})
        ]).start();
    };

    saveMacAddress(macAddress) {
        this.setState({macAddress: macAddress});
        storage.save(strings.WiFiMacaddress, macAddress);
    }

    forwardStep1() {
        if (this.state.macAddress?.length < 5 && constants.IS_IOS) {
            this.setState({
                showModal: true,
                ProgressErrors: true,
                configHint: "WiFi MacAddress is null"
            });
        } else {
            if (this.state.sessionSelected === 1) {
                this.setState({
                    ProgressErrors: false,
                    configHint: "",
                    time: 0,
                    maxDataUsage: 0
                });
                this.sendCommand(0, 0);
            } else {
                if (this.state.time !== 0 && this.state.maxDataUsage !== 0) {
                    this.setState({
                        ProgressErrors: false,
                        configHint: ""
                    });
                    this.sendCommand(this.state.time, this.state.maxDataUsage);
                } else {
                    this.setState({
                        ProgressErrors: true,
                        configHint: "Set Configuration First!"
                    });
                    this.startShake();
                }
            }
        }
    }

    backwardStep1() {
        this.setState({
            time: 0,
            maxDataUsage: 0,
            configHint: "",
            connectedToWifi: false,
        })
    }


    forwardStep2() {
        if (constants.IS_IOS) {
            Linking.canOpenURL('app-settings:').then(supported => {
                if (!supported) {
                    console.log('Can\'t handle settings url');
                } else {
                    return Linking.openURL('App-Prefs:WIFI');
                }
            }).catch(err => console.error('An error occurred', err));
        } else {
            this.connectWifi();
        }
    }

    backwardStep2() {
        this.setState({
            connectedToWifi: false,
        })
    }


    separator() {
        return <View style={{height: 1, backgroundColor: '#999999', marginLeft: px(20)}}/>;
    }

    onSelect = data => {
        this.setState({sessionData: data})
        let selectedButton = this.state.sessionData.find(e => e.selected == true);
        selectedButton = selectedButton ? selectedButton.value : this.state.sessionData[0].label;
        this.setState({sessionSelected: selectedButton === 'Temporary' ? 0 : 1})
    };

    render() {
        return (
            <View style={styles.container}>
                <Header searchBar rounded style={styles.headView}>
                    <TouchableOpacity
                        style={styles.centerAligned}
                        transparent
                        onPress={() => {
                            this.navReturn()
                        }}>
                        <Icon
                            type="MaterialIcons"
                            name='arrow-back'
                            style={{fontSize: moderateScale(26)}}
                        />
                    </TouchableOpacity>
                    <Body style={styles.centerAligned}>
                        <Title style={styles.headText}>
                            Via BLE
                        </Title>
                    </Body>
                </Header>
                {!this.state.isConnected &&
                <View style={styles.connectingContainer}>
                    <AnimatedSpread styles={styles.animation}/>
                    <Text style={styles.connectedInfo}>
                        {this.state.connectionStatus}
                    </Text>
                    <TouchableOpacity
                        style={styles.connectCorehubButton}
                        onPress={() => {
                            this.retryScanBLE();
                        }}>
                        <Text style={styles.buttonText}>Retry</Text>
                    </TouchableOpacity>
                </View>
                }
                {this.state.isConnected &&
                <ProgressSteps isComplete={this.state.connectedToWifi}
                               topOffset={px(10)}
                               labelFontSize={LabelText}
                               activeLabelFontSize={SmallText}
                               marginBottom={px(10)}>
                    <ProgressStep label="Configure Corehub"
                                  onNext={this.forwardStep1.bind(this)}
                                  errors={this.state.ProgressErrors}
                                  previousBtnTextStyle={{color: '#007aff', fontSize: MediumText}}
                                  nextBtnTextStyle={{color: '#007aff', fontSize: MediumText}}>

                        {constants.IS_IOS &&
                        <View>
                            <View style={styles.centeredView}>
                                <Modal
                                    animationType="slide"
                                    transparent={true}
                                    visible={this.state.showModal}
                                    onRequestClose={() => {
                                        Alert.alert("Modal has been closed.");
                                    }}
                                >
                                    <View style={styles.centeredView}>
                                        <View style={styles.modalView}>
                                            <Text style={styles.modalText}>Copy the Wi-Fi Address</Text>
                                            <View style={styles.hintView}>
                                                <Text style={styles.modalHintText}>
                                                    1. Go to settings
                                                </Text>
                                                <Text style={styles.modalHintText}>
                                                    2. Tap to General
                                                </Text>
                                                <Text style={styles.modalHintText}>
                                                    3. Tap About
                                                </Text>
                                                <Text style={styles.modalHintText}>
                                                    4. Find 'Wi-Fi Address', and long click to copy
                                                </Text>

                                            </View>

                                            <View style={styles.bottomView}>
                                                <TouchableHighlight
                                                    style={{...styles.openButton, backgroundColor: colors.CORETEX}}
                                                    onPress={() => {
                                                        Linking.canOpenURL('app-settings:').then(supported => {
                                                            if (!supported) {
                                                                console.log('Can\'t handle settings url');
                                                            } else {
                                                                return Linking.openURL('App-Prefs:General');
                                                            }
                                                        }).catch(err => console.error('An error occurred', err));
                                                    }}
                                                >
                                                    <Text style={styles.textStyle}>Go to Setting</Text>
                                                </TouchableHighlight>

                                                <TouchableHighlight
                                                    style={{...styles.openButton, backgroundColor: colors.CORETEX}}
                                                    onPress={() => {
                                                        this.setState({showModal: false});
                                                    }}
                                                >
                                                    <Text style={styles.textStyle}>Cancel</Text>
                                                </TouchableHighlight>
                                            </View>

                                        </View>
                                    </View>
                                </Modal>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({showModal: true});
                                    }}>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={styles.macaddressHint}>
                                            Your WiFi MacAddress:
                                        </Text>
                                        <Image
                                            style={styles.infoImage}
                                            source={Info}/>
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <TextInput placeholder="wifi mac address"
                                       underlineColorAndroid={'transparent'}
                                       style={styles.macaddress}
                                       value={this.state.macAddress}
                                       onChangeText={(macAddress) => this.saveMacAddress(macAddress)}/>
                        </View>
                        }

                        <RadioGroup
                            style={{marginTop: px(20)}}
                            radioButtons={this.state.sessionData}
                            onPress={this.onSelect}
                            flexDirection='row'
                        />

                        {this.state.sessionSelected === 0 &&
                        <Animated.View style={{transform: [{translateX: this.shakeAnimation}]}}>
                            <View style={{
                                width: '100%',
                                flex: 1,
                                paddingBottom: px(0),
                                marginTop: px(0),
                            }}>
                                <Text style={styles.setConfig}>
                                    Set hotspot duration:
                                </Text>
                                <Slider style={styles.picker}
                                        minimumValue={0}
                                        maximumValue={120}
                                        minimumTrackTintColor={colors.CORETEX}
                                        maximumTrackTintColor={colors.CORETEX}
                                        onValueChange={position => {
                                            this.setState({time: parseInt(position)});
                                        }}
                                />
                                <Text style={styles.duration}>
                                    {this.state.time} mins
                                </Text>
                                <Text style={styles.setConfig}>
                                    Set hotspot max data limit:
                                </Text>
                                <Slider style={styles.picker}
                                        minimumValue={0}
                                        maximumValue={100}
                                        minimumTrackTintColor={colors.CORETEX}
                                        maximumTrackTintColor={colors.CORETEX}
                                        onValueChange={position => {
                                            this.setState({maxDataUsage: parseInt(position)});
                                        }}
                                />
                                <Text style={styles.duration}>
                                    {this.state.maxDataUsage} M
                                </Text>
                                <Text style={styles.hintForConfig}>
                                    {this.state.configHint}
                                </Text>
                            </View>
                        </Animated.View>}

                        {this.state.sessionSelected !== 0 &&
                        <Text style={{
                            width: '100%',
                            height: px(180),
                            textAlign: 'center',
                            alignItems: 'center',
                            justifyContent: 'center',
                            textAlignVertical: "center",
                            marginTop: px(10)
                        }}>
                            No Time and Data Limit!
                        </Text>}
                    </ProgressStep>

                    <ProgressStep label="Connect to Hotspot"
                                  onNext={this.forwardStep2.bind(this)}
                                  onPrevious={this.backwardStep1.bind(this)}
                                  nextBtnText={this.state.isLoading ? "" : "Next"}
                                  previousBtnTextStyle={{color: '#007aff', fontSize: MediumText}}
                                  nextBtnTextStyle={{color: '#007aff', fontSize: MediumText}}>
                        <View style={{alignItems: 'center'}}>
                            <View style={{
                                width: '100%',
                                flex: 1,
                                paddingBottom: px(10),
                                paddingTop: px(10),
                                marginTop: px(10),
                            }}>
                                {this.state.isLoading &&
                                <View style={styles.loginIndicator}>
                                    <SkypeIndicator size={moderateScale(80)} color={colors.CORETEX}/>
                                </View>
                                }
                                <Text style={styles.currentState}>
                                    Current Status:
                                </Text>
                                <Text style={styles.connectInfoText}>
                                    {this.state.connectionStatus}
                                </Text>
                            </View>
                        </View>
                    </ProgressStep>
                    <ProgressStep label="Connected"
                                  onPrevious={this.backwardStep2.bind(this)}
                                  finishBtnText={" "}
                                  previousBtnTextStyle={{color: '#007aff', fontSize: MediumText}}
                                  nextBtnTextStyle={{color: '#007aff', fontSize: MediumText}}>
                        <View style={{alignItems: 'center'}}>
                            <View style={{
                                width: '100%',
                                flex: 1,
                                paddingBottom: px(20),
                                paddingTop: px(20),
                                marginTop: px(20),
                            }}>
                                {this.state.isLoading &&
                                <View style={styles.loginIndicator}>
                                    <SkypeIndicator size={moderateScale(80)} color={colors.CORETEX}/>
                                </View>
                                }
                                <Text style={styles.currentState}>
                                    Current Status:
                                </Text>
                                <Text style={styles.connectInfoText}>
                                    {this.state.connectionStatus}
                                </Text>
                            </View>
                        </View>
                    </ProgressStep>
                </ProgressSteps>
                }
            </View>
        )
    }
}


const styles = StyleSheet.create({
    connectingContainer: {
        flex: 1,
        width: "100%",
        height: "100%",
        alignItems: "center",
        backgroundColor: colors.WHITE,
    },
    animation: {
        flex: 1,
        alignItems: "center",
        justifyContent: "space-between",
    },
    connectCorehubButton: {
        backgroundColor: colors.CORETEX,
        borderRadius: 30,
        height: px(38),
        width: '75%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: px(10),
        marginBottom: px(20),
    },
    connectCorehubButtonDisable: {
        backgroundColor: colors.SILVER,
        borderRadius: 30,
        height: px(30),
        width: '75%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: px(10),
        marginBottom: px(10)
    },
    connectedInfo: {
        flex: 1,
        color: colors.CORETEX,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: SmallText,
        marginBottom: px(15),
        marginTop: px(20),
        paddingLeft: '10%',
        paddingRight: '10%',
    },
    buttonText: {
        color: colors.WHITE,
        textAlign: 'center',
        fontSize: SmallText,
    },
    stepText: {
        marginTop: px(10),
        marginRight: px(20),
    },
    loadingView: {
        marginTop: px(20),
        marginLeft: px(20),
    },
    stepTextDisable: {
        color: colors.DARKGREY,
        marginTop: px(10),
        marginRight: px(20),
    },
    containerS: {
        flex: 1,
        width: "100%",
        backgroundColor: colors.WHITE,
    },
    container: {
        flex: 1,
        width: "100%",
        height: "100%",
        backgroundColor: colors.WHITE,
        paddingTop: 0,
    },
    currentState: {
        width: '100%',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        color: colors.BLACK,
        fontSize: SmallText,
        marginTop: px(10),
    },
    connectInfoText: {
        color: colors.CORETEX,
        width: '100%',
        fontSize: SmallText,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: px(10)
    },
    setConfig: {
        width: '100%',
        color: colors.BLACK,
        fontSize: SmallText,
        marginTop: px(10),
        marginLeft: '10%',
    },
    hintForConfig: {
        color: colors.TORCH_RED,
        fontSize: SmallText,
        marginTop: px(20),
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    picker: {
        marginTop: px(10),
        width: '80%',
        marginLeft: '10%',
        marginRight: '10%',
        paddingLeft: '10%',
        paddingRight: '10%',
    },
    duration: {
        width: '100%',
        fontSize: SmallText,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    centerAligned: {alignItems: 'center', justifyContent: 'center'},
    header: {
        backgroundColor: colors.WHITE,
        alignItems: 'center',
    },
    headView: {
        width: '100%',
        height: px(50),
        backgroundColor: 'transparent',
    },
    headText: {
        fontSize: MediumText,
        color: colors.BLACK
    },
    loginIndicator: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        width: '80%',
        height: '50%',
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },

    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        marginLeft: px(10),
        marginRight: px(10),
        padding: px(10),
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    modalHintText: {
        marginBottom: 15,
        textAlign: "left",
    },
    infoImage: {
        width: px(20),
        height: px(20),
        marginTop: px(10),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    macaddressHint: {
        width: '80%',
        color: colors.BLACK,
        fontSize: SmallText,
        marginTop: px(10),
        marginLeft: '10%',
        justifyContent: 'flex-start'
    },
    bottomView: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        marginBottom: px(10),
        flexDirection: 'row'
    },
    hintView: {
        width: '100%',
        height: '100%',
    },
    macaddress: {
        width: '80%',
        borderRadius: 20,
        height: px(30),
        borderColor: colors.CORETEX,
        marginBottom: px(10),
        marginTop: px(10),
        paddingLeft: 10,
        padding: 0,
        borderWidth: 1,
        alignSelf: 'center',
        color: colors.BLACK
    },
});