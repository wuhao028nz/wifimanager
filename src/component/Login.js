import React, {Component} from 'react';
import {
    Dimensions,
    Image,
    ImageBackground,
    KeyboardAvoidingView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import logoImg from '../images/logo.png';
import colors from "../config/colors";
import strings from "../config/strings";
import constants from "../config/constants";
import {storage} from './storage/Storage'
import {SkypeIndicator} from 'react-native-indicators';
import {Form, Icon, Input, Item, ListItem} from 'native-base';
import CheckBox from 'react-native-check-box'
import {moderateScale} from "react-native-size-matters";
import SplashScreen from 'react-native-splash-screen'
import px from "./util/PX";
import {SmallText, MediumText, LargeText} from "./util/PX";

export default class Login extends Component {

    constructor() {
        super();
        const isPortrait = () => {
            const dim = Dimensions.get('screen');
            return dim.height >= dim.width;
        };

        this.state = {
            orientation: isPortrait() ? 'portrait' : 'landscape'
        };

        // Event Listener for orientation changes
        Dimensions.addEventListener('change', () => {
            this.setState({
                orientation: isPortrait() ? 'portrait' : 'landscape'
            });
        });
    }

    state: State = {
        email: "",
        password: "",
        loginButText: strings.LOGIN,
        isSelected: true,
        isLoading: false,
    };


    componentDidMount() {
        console.disableYellowBox = true;
        SplashScreen.hide();
        storage.load(strings.EMAIL_PLACEHOLDER, (data) => {
            this.setState({
                email: data,
            });
        });
        storage.load(strings.PASSWORD_PLACEHOLDER, (data) => {
            storage.load(strings.REMEMBER_KEY, (remember) => {
                this.setState({
                    password: data,
                });
                if (data !== "" && remember) {
                    this.props.navigation.replace('SelectCompanyView');
                }
            });
        });
    }

    handleSelectChange = () => {
        this.setState({isSelected: !this.state.isSelected});
    };

    handleEmailChange = (email: string) => {
        this.setState({email: email});
        storage.remove(strings.PASSWORD_PLACEHOLDER)
    };

    handlePasswordChange = (password: string) => {
        this.setState({password: password});
        storage.remove(strings.PASSWORD_PLACEHOLDER)
    };

    handleLoginPress = () => {
        this.setState({
            isLoading: true
        });
        this.setState({loginButText: "Loading"});
        fetch(constants.API_360 + "/Logon/UserLogin", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: this.state.email,
                password: this.state.password,
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(
                    "POST Response",
                    "Response Body -> " + JSON.stringify(responseData)
                );
                if (responseData.Code === 0) {
                    storage.save(strings.EMAIL_PLACEHOLDER, this.state.email);
                    storage.save(strings.PASSWORD_PLACEHOLDER, this.state.password);
                    if (typeof this.state.isSelected !== 'undefined') {
                        storage.save(strings.REMEMBER_KEY, this.state.isSelected);
                    }
                    storage.save(strings.SESSION_ID, responseData.Value.SessionID);
                    this.props.navigation.replace('SelectCompanyView');
                } else {
                    storage.remove(strings.PASSWORD_PLACEHOLDER);
                    alert(strings.INVALID_ACCESS);
                }
                this.setState({
                    isLoading: false
                });
            })
            .catch(error => {
                this.setState({
                    isLoading: false
                });
                alert(error);
            })
            .done();
    };


    render() {
        if (this.state.orientation === 'portrait') {
            return (
                <KeyboardAvoidingView
                    style={styles.containerWithStatus}
                    behavior={constants.IS_IOS ? "padding" : "height"}
                >
                    <View style={styles.container}>
                        <Image source={logoImg} style={styles.logo}/>
                        <View style={styles.form}>
                            <Item style={styles.floatingLable}>
                                <Icon active type="FontAwesome" name="user"/>
                                <Input
                                    placeholder={strings.EMAIL_PLACEHOLDER}
                                    editable={true}
                                    onChangeText={this.handleEmailChange}
                                    value={this.state.email}
                                />
                            </Item>
                            <Item style={styles.floatingLable}>
                                <Icon active type="FontAwesome" name="lock"/>
                                <Input
                                    placeholder={strings.PASSWORD_PLACEHOLDER}
                                    editable={true}
                                    onChangeText={this.handlePasswordChange}
                                    returnKeyType="next"
                                    secureTextEntry={true}
                                    value={this.state.password}
                                />
                            </Item>
                            <ListItem
                                style={styles.checkBox}>
                                <CheckBox color={colors.CORETEX}
                                          onClick={() => this.handleSelectChange()}
                                          checkBoxColor={colors.CORETEX}
                                          isChecked={this.state.isSelected}/>
                                <Text> Remember me</Text>
                            </ListItem>
                            <Form style={styles.Authenticationform}>
                                <Item style={styles.loginbuttonView}>
                                    <TouchableOpacity
                                        style={styles.loginButton}
                                        onPress={this.handleLoginPress.bind(this)}>
                                        <Text style={{color: colors.WHITE}}> Login </Text>
                                    </TouchableOpacity>
                                </Item>
                            </Form>
                        </View>
                        <ImageBackground
                            source={require('../images/background.png')}
                            style={{
                                flex: 1,
                                width: '100%',
                                marginTop: px(20),
                            }}
                        />
                    </View>
                    {this.state.isLoading &&
                    <View style={styles.loginIndicator}>
                        <SkypeIndicator size={moderateScale(80)} color={colors.CORETEX}/>
                    </View>
                    }
                </KeyboardAvoidingView>
            );
        } else {
            return (
                <KeyboardAvoidingView
                    style={styles.containerWithStatus}
                    behavior={constants.IS_IOS ? "padding" : "height"}
                >
                    <View style={styles.container}>
                        <Image source={logoImg} style={styles.logoLandscape}/>
                        <View style={styles.formLandscape}>

                            <Item style={styles.floatingLable}>
                                <Icon active type="FontAwesome" name="user"/>
                                <Input
                                    placeholder={strings.EMAIL_PLACEHOLDER}
                                    editable={true}
                                    onChangeText={this.handleEmailChange}
                                    value={this.state.email}
                                />
                            </Item>
                            <Item style={styles.floatingLable}>
                                <Icon active type="FontAwesome" name="lock"/>
                                <Input
                                    placeholder={strings.PASSWORD_PLACEHOLDER}
                                    editable={true}
                                    onChangeText={this.handlePasswordChange}
                                    returnKeyType="next"
                                    secureTextEntry={true}
                                    value={this.state.password}
                                />
                            </Item>
                            <ListItem
                                style={styles.checkBox}>
                                <CheckBox color={colors.CORETEX}
                                          onClick={() => this.handleSelectChange()}
                                          isChecked={this.state.isSelected}/>
                                <Text> Remember me</Text>
                            </ListItem>
                            <Form style={styles.Authenticationform}>
                                <Item style={styles.loginbuttonView}>
                                    <TouchableOpacity
                                        style={styles.loginButton}
                                        checkBoxColor={colors.CORETEX}
                                        onPress={this.handleLoginPress.bind(this)}>
                                        <Text style={{color: colors.WHITE}}> Login </Text>
                                    </TouchableOpacity>
                                </Item>
                            </Form>
                        </View>
                    </View>
                    {this.state.isLoading &&
                    <View style={styles.loginIndicator}>
                        <SkypeIndicator size={moderateScale(80)} color={colors.CORETEX}/>
                    </View>
                    }
                </KeyboardAvoidingView>
            );
        }

    }
}


const styles = StyleSheet.create({
    containerWithStatus: {
        flex: 3,
        width: "100%",
        backgroundColor: colors.CORETEX,
        alignItems: "center",
        justifyContent: "space-between",
        paddingTop: (constants.IS_IOS) ? px(18) : 0,
    },
    container: {
        flex: 3,
        width: "100%",
        backgroundColor: colors.WHITE,
        alignItems: "center",
        justifyContent: "space-between"
    },
    logo: {
        flex: 1,
        width: "80%",
        resizeMode: "contain",
        alignSelf: "center"
    },
    logoLandscape: {
        flex: 1,
        width: "40%",
        resizeMode: "contain",
        alignSelf: "center",
        marginTop: px(40)
    },
    text: {
        fontSize: LargeText,
        resizeMode: "contain",
        alignSelf: "center"
    },
    remmeberme: {
        fontSize: SmallText,
        marginTop: px(5),
        marginLeft: px(5),
        resizeMode: "contain",
    },
    form: {
        flex: 1,
        width: "80%",
        marginTop: px(10),
    },
    formLandscape: {
        flex: 2,
        width: "80%",
        marginTop: px(10),
    },
    checkboxContainer: {
        flex: 1,
        flexDirection: "row",
        marginBottom: px(5),
    },
    checkBox: {
        width: '100%',
        borderColor: 'transparent',
    },
    floatingLable: {
        width: '95%',
        borderColor: colors.SILVER,
    },
    loginIndicator: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF88',
    },
    Authenticationform: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    loginbuttonView: {
        alignSelf: 'center',
        width: '100%',
        borderColor: 'transparent',
        marginTop: px(15),
    },
    loginButton: {
        backgroundColor: colors.CORETEX,
        borderRadius: 20,
        height: constants.IS_IOS ? px(38) : px(38),
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
