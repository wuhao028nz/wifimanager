import React, {Component} from 'react';
import {
    FlatList,
    StyleSheet,
    View,
    Text,
    KeyboardAvoidingView,
    TouchableOpacity,
    Alert, RefreshControl
} from 'react-native';
import {CheckBox, Header, Item, Input, Icon, Root, ListItem} from 'native-base';
import colors from "../config/colors";
import strings from "../config/strings";
import constants from "../config/constants";
import {storage} from './storage/Storage'
import ApiService from "./util/ApiService";
import {moderateScale} from 'react-native-size-matters';
import {white} from "react-native-paper/src/styles/colors";
import {SkypeIndicator} from "react-native-indicators";
import px from "./util/PX";
import {SmallText, MediumText} from "./util/PX";

export default class SelectCompanyView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            companyList: [],
            companyToShow: [],
        };
    }

    componentDidMount() {
        this.getData();
    }

    async getData() {
        this.setState({isLoading: true});
        storage.load(strings.SESSION_ID, (data) => {
            this.fetchCompany(data)
        });
    }

    async fetchCompany(data) {
        const companyList = await ApiService.GetCompanyList(data);
        if (typeof companyList === 'string') {
            await this.setState({isLoading: false});
            alert(JSON.stringify(companyList));
        } else if (companyList.Value) {
            await this.setState({companyList: companyList.Value});
            await this.setState({companyToShow: companyList.Value});
            await this.setState({isLoading: false});
        } else if (!companyList.Value) {
            await this.setState({isLoading: false});
            if (companyList.Code === 1) {
                this.sessionExpire()
            } else {
                alert(JSON.stringify(companyList));
            }
        }
    }

    sessionExpire() {
        Alert.alert(
            'Session Expired',
            "Login is required",
            [
                {
                    text: 'OK',
                    onPress: () => {
                        storage.save(strings.REMEMBER_KEY, false);
                        this.props.navigation.replace('Login');
                    },
                },
                {
                    text: 'Cancel',
                    style: 'cancel',
                    onPress: () => {

                    },
                },
            ],
            {cancelable: false},
        );
    }

    separator() {
        return <View style={{height: 1, backgroundColor: '#999999', marginLeft: 20}}/>;
    }

    createListHeader() {
        return (
            <View style={styles.headView}>
                <Text style={{color: 'white'}}>
                    Select your company
                </Text>
            </View>
        )
    }

    onItemClick(item) {
        storage.save(strings.COMPANY_ID, item.ID);
        this.props.navigation.replace('SelectVehicleView');
    }

    onChangeText(text) {
        if (typeof text === 'string' && text.length > 0) {
            const newArr = this.state.companyList.filter(item => item.Name.toLowerCase().indexOf(text.toLowerCase()) > -1);
            this.setState({companyToShow: newArr})
        } else {
            this.setState({companyToShow: this.state.companyList})
        }
    }

    createListItem(item) {
        return (
            <TouchableOpacity activeOpacity={0.5} onPress={() => this.onItemClick(item)}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'flex-start',
                    justifyContent: 'flex-start',
                    marginTop: 5,
                    backgroundColor: '#fff',
                    padding: 10
                }}>
                    <Text style={{marginLeft: 6, padding: 12, fontSize: MediumText,}}>
                        {item.Name}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    navReturn() {
        storage.save(strings.REMEMBER_KEY, false);
        this.props.navigation.replace('Login');
    }

    render() {
        return (
            <KeyboardAvoidingView
                style={styles.container}
                behavior={constants.IS_IOS ? "padding" : "height"}
            >
                <View style={styles.container}>
                    <Header searchBar rounded style={styles.headView}>
                        <TouchableOpacity
                            style={styles.centerAligned}
                            transparent
                            onPress={() => {
                                this.navReturn()
                            }}>
                            <Icon
                                type="MaterialIcons"
                                name='arrow-back'
                                style={{fontSize: moderateScale(26)}}
                            />
                        </TouchableOpacity>
                        <Item
                            style={{
                                backgroundColor: colors.LIGHT_GREY,
                                borderRadius: moderateScale(25),
                                padding: moderateScale(2),
                            }}>
                            <Icon
                                type="MaterialIcons"
                                name="search"/>
                            <Input style={{fontSize: MediumText}}
                                   onChangeText={text => this.onChangeText(text)}
                                   placeholder="Search for your company"
                                   value={this.state.value}
                            />
                        </Item>
                    </Header>
                    <FlatList style={styles.flatList}
                              data={this.state.companyToShow}
                              renderItem={({item}) => this.createListItem(item)}
                              ItemSeparatorComponent={this.separator}
                              refreshControl={
                                  <RefreshControl
                                      title={"Loading"}
                                      colors={[colors.CORETEX]}
                                      tintColor={colors.CORETEX}
                                      titleColor={colors.CORETEX}
                                      refreshing={false}
                                      onRefresh={() => {
                                          this.getData();
                                      }}
                                  />
                              }
                    />
                    {this.state.isLoading &&
                    <View style={styles.loginIndicator}>
                        <SkypeIndicator size={moderateScale(80)} color={colors.CORETEX}/>
                    </View>
                    }
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        backgroundColor: colors.WHITE,
        paddingTop: 0,
    },
    headView: {
        width: '100%',
        height: px(50),
        backgroundColor: 'transparent'
    },
    searchBox: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    flatList: {
        width: '100%',
        backgroundColor: colors.WHITE,
    },
    centerAligned: {alignItems: 'center', justifyContent: 'center'},
    loginIndicator: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF88',
    },
});
