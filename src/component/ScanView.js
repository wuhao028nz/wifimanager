// import React, {Component} from 'react';
// import {
//     TouchableHighlight,
//     Modal,
//     Alert,
//     View,
//     Text,
//     TouchableOpacity,
//     Image,
//     StyleSheet,
//     PermissionsAndroid,
//     YellowBox
// } from 'react-native';
// import scanImg from "../images/scan.png";
// import colors from "../config/colors";
// import QRCodeScanner from 'react-native-qrcode-scanner';
// import {storage} from "./storage/Storage";
// import strings from "../config/strings";
//
// export default class ScanView extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             showCamera: false,
//             modalVisible: false,
//         };
//         YellowBox.ignoreWarnings(['Warning: ...']);
//     }
//
//     componentDidMount() {
//         this.getBlePermission()
//     }
//
//     getBlePermission = async () => {
//         const granted = await PermissionsAndroid.request(
//             PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
//             {
//                 title: 'Location Permission for BLE',
//                 message:
//                     'In order to use Bluetooth Low Energy, CoreHub Installer needs to access the device location. Do you wish to allow?',
//                 buttonNeutral: 'Ask Me Later',
//                 buttonNegative: 'Cancel',
//                 buttonPositive: 'OK',
//             },
//         );
//         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
//             // Check bluetooth state of device
//             const state = await this.manager.state();
//             if (state === 'PoweredOn') {
//                 return;
//             } else {
//                 await this.manager.enable();
//             }
//         } else {
//             console.log('BLE permission denied');
//         }
//     };
//
//     render() {
//         return (
//             <View style={styles.container}>
//                 {!this.state.showCamera && (
//                     <View style={styles.container}>
//                         <Image source={scanImg}
//                                style={styles.logo}
//                         />
//                         <Text style={styles.scanInfoText}>
//                             Please scan the QR code on your CoreHub unit in order to connect to it
//                         </Text>
//                         <TouchableOpacity
//                             style={styles.scanCorehubButton}
//                             onPress={() => {
//                                 this.setState({
//                                     showCamera: true,
//                                 });
//                             }}>
//                             <Text style={styles.buttonText}>Scan Now</Text>
//                         </TouchableOpacity>
//                     </View>
//                 )}
//
//                 {this.state.showCamera && (<QRCodeScanner
//                     style={styles.camera}
//                     markerStyle={{borderColor: colors.WHITE}}
//                     ref={node => {
//                         this.scanner = node;
//                     }}
//                     showMarker
//                     onRead={this.onSuccess.bind(this)}
//                     cameraProps={{
//                         captureAudio: false,
//                         flashMode: 'auto',
//                         autoFocus: 'on',
//                         autoFocusPointOfInterest: {x: 0.5, y: 0.5},
//                     }}
//                 />)}
//                 {<Modal
//                     animationType="slide"
//                     transparent={true}
//                     visible={this.state.modalVisible}
//                     onRequestClose={() => {
//                         this.setState({modalVisible:false})
//                     }}
//                 >
//                     <View style={styles.centeredView}>
//                         <View style={styles.modalView}>
//                             <Text style={styles.modalText}>Please choose one way to connect to this Corehub
//                                 device</Text>
//                             <TouchableHighlight
//                                 style={{...styles.openButton, backgroundColor: colors.CORETEX}}
//                                 onPress={() => {
//                                     this.setState({
//                                         modalVisible: !this.state.modalVisible
//                                     });
//                                     this.props.navigation.replace('BLEConnectView');
//                                 }}
//                             >
//                                 <Text style={styles.textStyle}>BLE</Text>
//                             </TouchableHighlight>
//
//                             <TouchableHighlight
//                                 style={{...styles.openButton, backgroundColor: colors.CORETEX}}
//                                 onPress={() => {
//                                     this.setState({
//                                         modalVisible: !this.state.modalVisible
//                                     });
//                                     this.props.navigation.replace('IoTmsgView');
//                                 }}
//                             >
//                                 <Text style={styles.textStyle}>IoThub</Text>
//                             </TouchableHighlight>
//
//                         </View>
//                     </View>
//                 </Modal>
//                 }
//             </View>
//         )
//     }
//
//     onSuccess = async e => {
//         console.log("scan result: " + JSON.stringify(e));
//         storage.save(strings.MAC_ADDRESS, JSON.stringify(e.data));
//         this.setState({
//             modalVisible: true
//         })
//     }
// }
//
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         width: "100%",
//         backgroundColor: colors.WHITE,
//         alignItems: "center",
//         justifyContent: "space-between"
//     },
//     logo: {
//         flex: 5,
//         width: "100%",
//         resizeMode: "contain",
//         alignSelf: "center",
//     },
//     scanInfoText: {
//         flex: 1,
//         marginTop: 50,
//         color: colors.BLACK,
//         textAlign: 'center',
//         fontSize: 13,
//         marginBottom: 15,
//         paddingLeft: '25%',
//         paddingRight: '25%',
//     },
//     scanCorehubButton: {
//         flex: 1,
//         backgroundColor: colors.CORETEX,
//         borderRadius: 30,
//         height: 38,
//         width: '75%',
//         alignItems: 'center',
//         justifyContent: 'center',
//         marginTop: 10,
//         marginBottom: 50
//     },
//     buttonText: {
//         color: colors.WHITE,
//         textAlign: 'center',
//         fontSize: 14,
//     },
//     camera: {
//         flex: 1,
//         width: "100%",
//         resizeMode: "contain",
//     },
//     centeredView: {
//         flex: 1,
//         justifyContent: "center",
//         alignItems: "center",
//         marginTop: 22
//     },
//     modalView: {
//         margin: 20,
//         backgroundColor: "white",
//         borderRadius: 20,
//         padding: 35,
//         alignItems: "center",
//         shadowColor: "#000",
//         shadowOffset: {
//             width: 0,
//             height: 2
//         },
//         shadowOpacity: 0.25,
//         shadowRadius: 3.84,
//         elevation: 5
//     },
//     openButton: {
//         width: '100%',
//         marginTop: 10,
//         backgroundColor: "#F194FF",
//         borderRadius: 20,
//         padding: 10,
//         elevation: 2,
//         marginLeft: '10%',
//         marginRight: '10%'
//     },
//     textStyle: {
//         width: 300,
//         color: "white",
//         fontWeight: "bold",
//         textAlign: "center"
//     },
//     modalText: {
//         marginBottom: 15,
//         textAlign: "center"
//     }
// });
