import {PixelRatio} from 'react-native';

const dp2px = dp => PixelRatio.getPixelSizeForLayoutSize(dp);
const px2dp = px => PixelRatio.roundToNearestPixel(px);

export default function px(size) {
    return px2dp(size);
}

export const LabelText = PixelRatio.getFontScale() * 12;
export const SmallText = PixelRatio.getFontScale() * 14;
export const MediumText = PixelRatio.getFontScale() * 16;
export const LargeText = PixelRatio.getFontScale() * 26;