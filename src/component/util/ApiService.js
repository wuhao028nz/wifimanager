import constants from "../../config/constants";

export default class API {


    static async userLogin(APIEndpoint, uName, pwd) {
        try {
            const response = await fetch(constants.API_360 + `/Logon/UserLogin`, {
                method: 'POST',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: uName,
                    password: pwd,
                }),
            });
            const responseOK = response && response.ok;
            if (responseOK) {
                return await response.json();
            }
            return 'Unknown';
        } catch (e) {
            return e.message;
        }
    }


    static async GetCompanyList(session) {
        try {
            const response = await fetch(constants.API_360 + `/Company/GetCompanyList`, {
                method: 'GET',
                headers: {
                    SessionID: session,
                    'Content-Type': 'application/json',
                },
            });
            const responseOK = response && response.ok;
            if (responseOK) {
                return await response.json();
            }
            return 'Unknown';
        } catch (e) {
            return e.message;
        }
    }

    static async getCorehubVehicleList(session, companyID) {
        try {
            const vehicles = await fetch(
                constants.API_360 + `/Vehicle/CorehubVehicleList?companyID=${companyID}`,
                {
                    method: 'GET',
                    headers: {
                        SessionID: session,
                        'Content-Type': 'application/json',
                    },
                },
            );
            const responseOK = vehicles && vehicles.ok;
            if (responseOK) {
                return await vehicles.json();
            }
            return 'Unknown';
        } catch (e) {
            return e.message;
        }
    }

    static async sendMsg(deviceID, sessionID, app, type, data) {
        try {
            let result = await fetch(constants.REACT_APP_COREHUB_SUPPORT_API + `corehubSupport/sendMsg?trackID=${deviceID}&app=${app}&type=${type}&data=${data}`,
                {
                    method: 'GET',
                    headers: {
                        SessionID: sessionID,
                        'Content-Type': 'application/json',
                    },
                }
            )
                .then(res => res.json());
            console.log("get the sendMsg details: " + JSON.stringify(result));
            return result;
        } catch (e) {
            console.log('error while sendMsg:', e);
        }
    }

    static async getHotspotInfo(deviceID, sessionID) {
        try {
            let result = await fetch(constants.REACT_APP_COREHUB_SUPPORT_API + `corehubSupport/LoadHotspotInfo/${deviceID}`,
                {
                    method: 'GET',
                    headers: {
                        SessionID: sessionID,
                        'Content-Type': 'application/json',
                    },
                }
            )
                .then(res => res.json());
            console.log("get the getHotspotInfo details: " + JSON.stringify(result));
            return result;
        } catch (e) {
            console.log('error while getHotspotInfo:', e);
        }
    }

    static async isConnected(deviceID, sessionID) {
        try {
            let result = await fetch(constants.REACT_APP_COREHUB_SUPPORT_API + `corehubSupport/isConnected/${deviceID}`,
                {
                    method: 'GET',
                    headers: {
                        SessionID: sessionID,
                        'Content-Type': 'application/json',
                    },
                }
            )
                .then(res => res.json());
            console.log("get the isConnected details: " + JSON.stringify(result));
            return result;
        } catch (e) {
            console.log('errors while isConnected:', e);
        }
    }

}