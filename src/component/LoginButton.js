import * as React from "react";
import {StyleSheet, Text, TouchableOpacity} from "react-native";
import colors from "../config/colors";

interface Props {
    label: string;
    onPress: () => void;
}

class LoginButton extends React.Component<Props> {
    render() {
        const {label, onPress} = this.props;
        return (
            <TouchableOpacity style={styles.container} onPress={onPress}>
                <Text style={styles.text}>{label}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: colors.CORETEX,
        marginBottom: 12,
        paddingVertical: 12,
        borderRadius: 20,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: colors.CORETEX
    },
    text: {
        color: colors.WHITE,
        textAlign: "center",
        height: 20
    }
});
export default LoginButton;
